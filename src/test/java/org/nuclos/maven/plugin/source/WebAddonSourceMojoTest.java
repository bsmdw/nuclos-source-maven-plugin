package org.nuclos.maven.plugin.source;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.io.FileMatchers.anExistingDirectory;
import static org.hamcrest.io.FileMatchers.anExistingFile;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.nio.file.Path;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.project.MavenProject;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class WebAddonSourceMojoTest extends AbstractMojoTestCase {

    @Mock
    private MavenProject project;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testBasicEmptySetup() throws Exception {
        WebAddonSourceMojo mojo = (WebAddonSourceMojo) lookupMojo("webaddon", new File( getBasedir(),
                "src/test/resources/unit/nuclos-source-basic/plugin-config.xml" ));

        File outputDir = new File(".");

        setVariableValueToObject(mojo, "project", project);
        setVariableValueToObject(mojo, "outputDirectory", outputDir);

        Properties props = new Properties();
        when(project.getProperties()).thenReturn(props);

        mojo.execute();

        verify(project, times(1)).getProperties();
        assertThat(props.size(), equalTo(1));
        assertThat(props.stringPropertyNames(), hasItem("nuclos.source.web.sourcedir"));
        assertThat(props.getProperty("nuclos.source.web.sourcedir"), equalTo(outputDir.toPath().toAbsolutePath().toString()));
    }

    @Test
    public void testInvalidNuclet() throws Exception {
        WebAddonSourceMojo mojo = (WebAddonSourceMojo) lookupMojo("webaddon", new File( getBasedir(),
                "src/test/resources/unit/nuclos-source-invalid-nuclet/plugin-config.xml" ));

        setVariableValueToObject(mojo, "failIfNucletInvalid", true);

        thrown.expect(MojoFailureException.class);
        thrown.expectMessage(containsString("does not represent a valid Nuclet location"));
        mojo.execute();
    }

    @Test
    public void testSkip() throws Exception {
        WebAddonSourceMojo mojo = (WebAddonSourceMojo) lookupMojo("webaddon", new File( getBasedir(),
                "src/test/resources/unit/nuclos-source-skip/plugin-config.xml" ));

        mojo.execute();
        assertThat(mojo.documentBuilder, is(nullValue()));
    }

    @Test
    public void testMultipleNuclets() throws Exception {
        WebAddonSourceMojo mojo = (WebAddonSourceMojo) lookupMojo("webaddon", new File( getBasedir(),
                "src/test/resources/unit/nuclos-source-multiple-nuclets/plugin-config.xml" ));

        File outputDir = new File("target/test-generated/nuclos-source-multiple-nuclets");

        setVariableValueToObject(mojo, "project", project);
        setVariableValueToObject(mojo, "outputDirectory", outputDir);

        Properties props = new Properties();
        when(project.getProperties()).thenReturn(props);

        mojo.execute();

        Path outputPath = outputDir.toPath().resolve("addons");
        // Assert that first Nuclet is written to disk
        assertThat(outputPath.resolve("another-test-addon").toFile(), anExistingDirectory());
        // Make sure that the path elements in web addon file definition is respected
        assertThat(outputPath.resolve("second-test-addon").resolve("src").resolve("index.ts").toFile(), anExistingFile());
    }

    @Test
    public void testExcludeAddons() throws Exception {
        WebAddonSourceMojo mojo = (WebAddonSourceMojo) lookupMojo("webaddon", new File( getBasedir(),
                "src/test/resources/unit/nuclos-source-exclude-webaddon/plugin-config.xml" ));

        File outputDir = new File("target/test-generated/nuclos-source-exclude-webaddon");

        setVariableValueToObject(mojo, "project", project);
        setVariableValueToObject(mojo, "outputDirectory", outputDir);

        Properties props = new Properties();
        when(project.getProperties()).thenReturn(props);

        mojo.execute();

        Path outputPath = outputDir.toPath().resolve("addons");
        assertThat(outputPath.resolve("another-test-addon").toFile(), not(anExistingDirectory()));
    }

    @Test
    public void testActiveFilter() throws Exception {
        WebAddonSourceMojo mojo = (WebAddonSourceMojo) lookupMojo("webaddon", new File( getBasedir(),
                "src/test/resources/unit/nuclos-source-webaddon-skipinactive/plugin-config.xml" ));

        File outputDir = new File("target/test-generated/nuclos-source-webaddon-skipinactive");

        setVariableValueToObject(mojo, "project", project);
        setVariableValueToObject(mojo, "outputDirectory", outputDir);

        Properties props = new Properties();
        when(project.getProperties()).thenReturn(props);

        mojo.execute();

        Path outputPath = outputDir.toPath().resolve("addons");
        assertThat(outputPath.resolve("another-test-addon").toFile(), anExistingDirectory());
        assertThat(outputPath.resolve("second-test-addon").toFile(), not(anExistingDirectory()));
    }

    @Test
    public void testCreateRegistry() throws Exception {
        WebAddonSourceMojo mojo = (WebAddonSourceMojo) lookupMojo("webaddon", new File( getBasedir(),
                "src/test/resources/unit/nuclos-source-webaddon-skipinactive/plugin-config.xml" ));

        File outputDir = new File("target/test-generated/nuclos-source-webaddon-registry");

        setVariableValueToObject(mojo, "project", project);
        setVariableValueToObject(mojo, "outputDirectory", outputDir);
        setVariableValueToObject(mojo, "createModulesRegistry", true);

        Properties props = new Properties();
        when(project.getProperties()).thenReturn(props);

        mojo.execute();

        Path outputPath = outputDir.toPath().resolve("app");
        File registryFile = outputPath.resolve("addon.modules.ts").toFile();
        assertThat(registryFile, anExistingFile());
        assertThat(FileUtils.readFileToString(registryFile), containsString("import { AnotherTestAddonModule }"));
    }
}
