package org.nuclos.maven.plugin.source;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.project.MavenProject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RuleSourceMojoTest extends AbstractMojoTestCase {
    @Mock
    private MavenProject project;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSimpleRuleExport() throws Exception {
        RuleSourceMojo mojo = (RuleSourceMojo) lookupMojo("rule", new File(getBasedir(),
                "src/test/resources/unit/nuclos-source-rule/plugin-config.xml" ));

        File outputDir = new File("target/test-generated/nuclos-source-rule");

        setVariableValueToObject(mojo, "project", project);
        setVariableValueToObject(mojo, "outputDirectory", outputDir);

        mojo.execute();

        verify(project, times(1)).addCompileSourceRoot(stringCaptor.capture());
        assertThat(Paths.get(stringCaptor.getValue()), equalTo(Paths.get("target/test-generated/nuclos-source-rule/rules")));
        Path outputPath = outputDir.toPath().toAbsolutePath();
        assertThat(outputPath.resolve("rules/example/rest/SendOrder.java").toFile().exists(), is(true));
    }
}
