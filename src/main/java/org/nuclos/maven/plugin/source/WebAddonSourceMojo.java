package org.nuclos.maven.plugin.source;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuclos.api.ide.utils.WebAddonUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Write all Nuclos web addons defined inside Nuclets and that are active to a directory
 * <p>
 * Searches all defined Nuclet locations for web addons marked as active and writes the sources to a target directory
 * you can define using the <code>outputDirectory</code> parameter. In addition the Nuclos core webclient can be added
 * and a module registry created based on the found web addons. This will allow you to simply build the whole web client
 * using NPM.
 */
@Mojo(name = "webaddon", defaultPhase = LifecyclePhase.GENERATE_SOURCES, threadSafe = true,
        requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class WebAddonSourceMojo extends AbstractNuclosSourceMojo {

    final private static String OUTPUT_DIRECTORY_PROPERTY = "nuclos.source.web.sourcedir";

    /**
     * Change registry entries in NPM RC files globally
     * <p>
     * If set this parameter will be set in any registry definition in any .npmrc file found in the Nuclet
     */
    @Parameter
    private String registryReplacement = null;

    /**
     * Writes out a module registry containing all web addons
     */
    @Parameter(defaultValue = "false")
    private boolean createModulesRegistry = false;

    /**
     * Configures source files to be production ready
     * <p>
     * Output of source files will be prepared to be production ready, i.e. file references in module registry will not
     * be referencing source files, but module names.
     */
    @Parameter(property = "nuclos.source.webaddon.devmode", defaultValue = "false")
    private boolean devMode = false;

    /**
     * Retrieves the Nuclos core web client and adds it to the output directory
     */
    @Parameter(defaultValue = "false")
    private boolean fetchCoreWebclient = false;

    /**
     * Exclude web addons from the list of sources to write to disk; allows '*' wildcards
     */
    @Parameter
    private Set<String> excludedAddons = Collections.emptySet();

    private Set<Pattern> compiledExcludeFilters = Collections.emptySet();

    @Override
    protected void run() throws MojoExecutionException {
        Path sourceLocation = getNuclosSourceOutputDirectory().toPath().toAbsolutePath();

        Set<WebAddonDefinition> activeWebAddons = new HashSet<>();

        // Set the target directory as a Maven property for subsequent usage by other plugins
        setOutputDirectoryProperty(sourceLocation);

        // Compile exclude filters
        compiledExcludeFilters = compileFilters(excludedAddons);

        // Loop over all defined Nuclets and list the defined web addons marked as active
        for (File nuclet : Optional.ofNullable(nuclets).orElse(Collections.emptyList())) {
            for (File webAddonDefinitionFile : getAllWebAddonDefinitionsFiles(nuclet)) {
                WebAddonDefinition webAddonDefinition = readDefinition(webAddonDefinitionFile, nuclet.toPath());

                if (webAddonDefinition != null && webAddonDefinition.isActive()) {
                    activeWebAddons.add(webAddonDefinition);
                }
            }
        }

        // Filtering...
        int activeWebAddonCount = activeWebAddons.size();
        activeWebAddons.removeIf(webAddonDefinition -> matchesFilter(webAddonDefinition.getName(), compiledExcludeFilters));

        if (getLog().isDebugEnabled()) {
            getLog().debug("Removed " + (activeWebAddonCount - activeWebAddons.size()) + " web addons by " +
                    "applying excludes");
        }

        // Now work through the list of web addons that are active and not filtered out
        for (WebAddonDefinition webAddonDefinition : activeWebAddons) {
            for (WebAddonFile webAddonFile : webAddonDefinition.getSourceFiles()) {
                File webAddonSourceFile = Paths.get(webAddonFile.getPath()).toFile();

                if (!webAddonSourceFile.exists() || !webAddonSourceFile.isFile() || !webAddonSourceFile.canRead()) {
                    if (getLog().isWarnEnabled()) {
                        getLog().warn("Web addon file '" + webAddonSourceFile.getName() + "' is defined in " +
                                "Nuclet but source file is not accessible");
                    }
                } else {
                    // Write addon file to: ${basedir}/target/<generated>/addons/<Addon-Name>/<filename>
                    Path webAddonFileDest = sourceLocation.resolve("addons")
                            .resolve(WebAddonUtils.camelCaseToFileName(webAddonDefinition.getName()))
                            .resolve(webAddonFile.getName());

                    try {
                        if (getLog().isDebugEnabled()) {
                            getLog().debug("Copying web addon file " + webAddonSourceFile.getName() + " to " +
                                    "target " + webAddonFileDest.toString());
                        }

                        // Assert directories in target path
                        Files.createDirectories(webAddonFileDest.getParent());
                        Files.copy(webAddonSourceFile.toPath(), webAddonFileDest, StandardCopyOption.REPLACE_EXISTING);

                        // If this is a .npmrc file, adapt it if necessary (registry)
                        if (webAddonFileDest.getFileName().toString().equals(".npmrc") && StringUtils.isNotEmpty(registryReplacement)) {
                            replaceRegistryInNpmrc(webAddonFileDest);
                        }
                    } catch (IOException e) {
                        throw new MojoExecutionException("Could not copy web addon source to " + webAddonFileDest);
                    }
                }
            }
        }

        // Is this run configured to build a module registry?
        if (createModulesRegistry) {
            createModulesRegistryFile(activeWebAddons, sourceLocation);
        }

        if (fetchCoreWebclient) {
            downloadNuclosCoreWebclient();
        }
    }

    private void downloadNuclosCoreWebclient() {
        // TODO: implement this once clear how to retrieve sources
    }

    private void setOutputDirectoryProperty(Path outputDirectory) {
        if (getLog().isDebugEnabled()) {
            getLog().debug("Setting Maven property '" + OUTPUT_DIRECTORY_PROPERTY + "' to " +
                    outputDirectory.toAbsolutePath().toString());
        }

        project.getProperties().setProperty(OUTPUT_DIRECTORY_PROPERTY, outputDirectory.toAbsolutePath().toString());
    }

    void createModulesRegistryFile(Set<WebAddonDefinition> activeWebAddons, Path sourceLocation) {
        if (getLog().isDebugEnabled()) {
            getLog().debug("Building a module registry");
        }

        WebAddonUtils.generateWebAddonModulesRegistry(
                activeWebAddons.stream()
                        .map(WebAddonDefinition::getName)
                        .collect(Collectors.toSet()),
                devMode,
                sourceLocation.resolve("app/addon.modules.ts"));
    }

    void replaceRegistryInNpmrc(Path npmrc) throws MojoExecutionException {
        try {
            if (getLog().isDebugEnabled()) {
                getLog().debug("Altering registry entries in .npmrc file at " + npmrc.toString());
            }

            String npmRcContent = new String(Files.readAllBytes(npmrc));
            npmRcContent.replaceAll("registry\\s*=.*$", "registry = " + registryReplacement);
            Files.write(npmrc, npmRcContent.getBytes());
        } catch (IOException e) {
            throw new MojoExecutionException("Could not alter .npmrc file in '" + npmrc.toString() + "'", e);
        }
    }

    Collection<File> getAllWebAddonDefinitionsFiles(File nuclet) {
        Set<File> webAddonDefinitions = new HashSet<>();

        for (File nucletNamespace : Optional.ofNullable(nuclet.listFiles(((dir, name) -> dir.isDirectory() && dir.canRead()
                /*&& StringUtils.startsWith(name, "org.nuclet")*/))).orElse(new File[0])) {
            getLog().debug("Checking Nuclet namespace '" + nucletNamespace.getAbsolutePath() + "' for webaddon definitions");

            File webAddonDir = Paths.get(nucletNamespace.getPath()).resolve("nuclos_webaddon").toFile();
            if (webAddonDir.exists()) {
                // List all web addon directories
                Arrays.stream(webAddonDir.listFiles(item -> item != null && item.isDirectory() && item.canRead()))
                        // Now process each addon dir and remember its config
                        .forEach(webAddonFilesDir -> webAddonDefinitions.addAll(
                                Arrays.asList(webAddonFilesDir.listFiles((file, name) -> StringUtils.endsWith(name, ".nuclos_webaddon.eoml")))));
            }
        }

        getLog().debug("Found " + webAddonDefinitions.size() + " web addon definitions in Nuclet");
        return Collections.unmodifiableSet(webAddonDefinitions);
    }

    WebAddonDefinition readDefinition(File input, Path nuclet) {
        try {
            Document doc = documentBuilder.parse(input);

            WebAddonDefinition webaddonDefinition = new WebAddonDefinition();
            String name = (String) xPath.evaluate("eo[@entity='nuclos_webAddon']/name/text()", doc, XPathConstants.STRING);
            webaddonDefinition.setName(name);

            webaddonDefinition.setActive(Boolean.valueOf(
                    (String) xPath.evaluate("eo[@entity='nuclos_webAddon']/active/text()", doc, XPathConstants.STRING)));

            File webAddonFilesDir = Paths.get(input.getParent()).resolve("nuclos_webaddonfile").toFile();
            if (webAddonFilesDir.exists() && webAddonFilesDir.isDirectory() && webAddonFilesDir.canRead()) {
                for (File sourceDefinition : webAddonFilesDir.listFiles((file, sourceDefinition) -> StringUtils.endsWith(sourceDefinition, ".nuclos_webaddonfile.eoml"))) {
                    doc = documentBuilder.parse(sourceDefinition);
                    WebAddonFile webAddonFile = new WebAddonFile();
                    String fileName = (String) xPath.evaluate("eo[@entity='nuclos_webAddonFile']/file/filename/text()", doc, XPathConstants.STRING);

                    try {
                        String path = (String) xPath.evaluate("eo[@entity='nuclos_webAddonFile']/path/text()", doc, XPathConstants.STRING);
                        if (StringUtils.isNotEmpty(path)) {
                            fileName = Paths.get(path).resolve(fileName).toString();
                        }
                    } catch (XPathExpressionException e) {
                        // Simply ignore this...
                    }

                    webAddonFile.setName(fileName);
                    String sourceName = (String) xPath.evaluate("eo[@entity='nuclos_webAddonFile']/__externalizedFiles__/file/text()", doc, XPathConstants.STRING);
                    webAddonFile.setPath(nuclet.resolve(sourceName).toAbsolutePath().toString());
                    webaddonDefinition.addSourceFile(webAddonFile);
                }
            } else {
                getLog().warn("Found web addon definition for '" + name + "' but it does not seem to have any sources");
            }

            return webaddonDefinition;
        } catch (SAXException | IOException | XPathExpressionException e) {
            if (getLog().isWarnEnabled()) {
                getLog().warn("Could not parse web addon definition file " + input);
            }

            return null;
        }
    }

    private class WebAddonDefinition {
        private boolean active;
        private Set<WebAddonFile> sourceFiles = new HashSet<>();
        private String name;

        String getName() {
            return name;
        }

        void setName(String name) {
            this.name = name;
        }

        boolean isActive() {
            return active;
        }

        void setActive(boolean active) {
            this.active = active;
        }

        Set<WebAddonFile> getSourceFiles() {
            return sourceFiles;
        }

        void addSourceFile(WebAddonFile file) {
            sourceFiles.add(file);
        }
    }

    private class WebAddonFile {
        private String name;
        private String path;

        String getName() {
            return name;
        }

        void setName(String name) {
            this.name = name;
        }

        String getPath() {
            return path;
        }

        void setPath(String path) {
            this.path = path;
        }
    }
}
