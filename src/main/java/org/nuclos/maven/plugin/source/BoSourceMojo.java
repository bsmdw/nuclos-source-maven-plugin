package org.nuclos.maven.plugin.source;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

@Mojo(name = "bo", defaultPhase = LifecyclePhase.GENERATE_SOURCES, threadSafe = true,
    requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class BoSourceMojo extends AbstractNuclosSourceMojo {

    @Override
    public void run() throws MojoExecutionException, MojoFailureException {
    }

}
