package org.nuclos.maven.plugin.source;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.io.FilenameUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

@Mojo(name = "rule", defaultPhase = LifecyclePhase.GENERATE_SOURCES, threadSafe = true,
        requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class RuleSourceMojo extends AbstractNuclosSourceMojo {

    final private static String RULE_TARGET_DIR = "rules";

    @Override
    public void run() throws MojoExecutionException, MojoFailureException {
        Path sourceLocation = getNuclosSourceOutputDirectory().toPath().resolve(RULE_TARGET_DIR);
        addTemporarySourceDirectory(sourceLocation);

        for (File nuclet : nuclets) {
            for (File ruleDefinitionFile : getAllRuleDefinitionsFiles(nuclet)) {
                RuleDefinition ruleDefinition = readDefinition(ruleDefinitionFile);

                if (ruleDefinition != null && ruleDefinition.isActive()) {
                    File ruleSource = nuclet.toPath().resolve(ruleDefinition.getSourceFile()).toFile();

                    if (!ruleSource.exists() || !ruleSource.isFile() || !ruleSource.canRead()) {
                        if (getLog().isWarnEnabled()) {
                            getLog().warn("Rule '" + ruleDefinition.getCanonicalName() + "' is defined in Nuclet but source file is not accessible");
                        }
                    } else {
                        Path ruleDest = sourceLocation.resolve(ruleDefinition.getPath());
                        try {
                            Files.createDirectories(ruleDest.getParent());
                            Files.copy(ruleSource.toPath(), ruleDest, StandardCopyOption.REPLACE_EXISTING);

                            if (getLog().isDebugEnabled()) {
                                getLog().debug("Copying rule " + ruleDefinition.getCanonicalName() +
                                        " to target " + ruleDest.toAbsolutePath().toString());
                            }
                        } catch (IOException e) {
                            throw new MojoExecutionException("Could not copy rule source to " + ruleDest, e);
                        }
                    }
                }
            }
        }
    }

    protected Collection<File> getAllRuleDefinitionsFiles(File nuclet) {
        Set<File> ruleDefinitions = new HashSet<>();

        for (File nucletNamespace : Optional.ofNullable(nuclet.listFiles(((dir, name) -> dir.isDirectory()
                && dir.canRead() /*&& name.startsWith("org.nuclet")*/))).orElse(new File[0])) {
            getLog().debug("Checking Nuclet namespace '" + nucletNamespace.getAbsolutePath() + "' for rule definitions");

            File serverCodeDir = new File(FilenameUtils.concat(nucletNamespace.getPath(), "nuclos_servercode"));
            if (serverCodeDir.exists()) {
                ruleDefinitions.addAll(Arrays.asList(Optional.ofNullable(
                        serverCodeDir.listFiles((file, name) -> name.endsWith(".nuclos_servercode.eoml")))
                        .orElse(new File[0])));
            }
        }

        getLog().debug("Found " + ruleDefinitions.size() + "  rule definitions in Nuclet");
        return Collections.unmodifiableSet(ruleDefinitions);
    }

    protected RuleDefinition readDefinition(File input) {
        try {
            Document doc = documentBuilder.parse(input);

            RuleDefinition ruleDefinition = new RuleDefinition();
            String name = (String) xPath.evaluate("eo[@entity='nuclos_servercode']/name/text()", doc, XPathConstants.STRING);
            ruleDefinition.setCanonicalName(name);

            ruleDefinition.setActive(Boolean.valueOf(
                    (String) xPath.evaluate("eo[@entity='nuclos_servercode']/active/text()", doc, XPathConstants.STRING)));

            ruleDefinition.setSourceFile((String) xPath.evaluate("eo[@entity='nuclos_servercode']/__externalizedFiles__/source/text()", doc, XPathConstants.STRING));

            return ruleDefinition;
        } catch (SAXException | IOException | XPathExpressionException e) {
            if (getLog().isWarnEnabled()) {
                getLog().warn("Could not parse rule definition fiile " + input);
            }

            return null;
        }
    }

    private class RuleDefinition {
        private String canonicalName;
        private boolean active;
        private String sourceFile;

        String getCanonicalName() {
            return canonicalName;
        }

        void setCanonicalName(String canonicalName) {
            this.canonicalName = canonicalName;
        }

        String getPackageName() {
            return canonicalName.substring(0, canonicalName.lastIndexOf('.'));
        }

        String getClassName() {
            return canonicalName.substring(canonicalName.lastIndexOf('.') + 1);
        }

        String getPath() {
            return getPackageName().replace('.', File.separatorChar) + File.separatorChar + getClassName() + ".java";
        }

        boolean isActive() {
            return active;
        }

        void setActive(boolean active) {
            this.active = active;
        }

        String getSourceFile() {
            return sourceFile;
        }

        void setSourceFile(String sourceFile) {
            this.sourceFile = sourceFile;
        }
    }

}
