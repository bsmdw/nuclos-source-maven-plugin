Build Nuclos webclient including addons
============
A common scenario for customized Nuclos instances is to create server and/or client extensions and maybe also web addons
if you are opting for the newer web version of the Nuclos client. Building these customizations is usually performed in
a dedicated build project, e.g. a Maven or Gradle project. This article is specifically dedicated to setting up a Maven
project for building the Nuclos web client including web addons that are stored in a Nuclet.

Extract the web addon sources from Nuclet(s)
------------
In order to build a custom Nuclos project using the web client and add additional web addons the sources of these
addons need to be extracted from the Nuclet(s). The Nuclos Sources Plugin can be configured to extract the sources
and write them to a target folder where they can be built using `npm`.

By default the files are written to `\${project.build.directory}/generated-sources/nuclos-sources` but
this can be changed using the plugin configuration parameter `outputDirectory`.
```xml
<project>
    [...]
    <build>
        [...]
        <plugins>
            <plugin>
                <groupId>org.nuclos</groupId>
                <artifactId>nuclos-source-maven-plugin</artifactId>
                <version>${project.version}</version>
                <configuration>
                    <nuclets>
                        <nuclet>/path/to/my/first/nuclet</nuclet>
                        <nuclet>../path/to/second/nuclet</nuclet>
                    </nuclets>
                    <createModulesRegistry>true</createModulesRegistry>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>webaddon</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        [...]
    </build>
    [...]
</project>
```
After the files are written to the output folder you can now invoke a NPM build, for instance using the
[frontend-maven-plugin](https://github.com/eirslett/frontend-maven-plugin). For configuration you can use the
property `${nuclos.source.web.sourcedir}` which is set by the Nuclos Source Plugin. The frontend plugin will
download a NodeJS environment, handle all proxy and tool configuration and will invoke the NPM calls defined in the
executions:
```xml
<project>
    [...]
    <build>
        [...]
        <plugins>
            [...]
            <plugin>
                <groupId>com.github.eirslett</groupId>
                <artifactId>frontend-maven-plugin</artifactId>
                <version>1.6</version>
                <configuration>
                    <!-- This property is set by the nuclos-source-maven-plugin according to your configuration -->
                    <workingDirectory>${nuclos.source.web.sourcedir}</workingDirectory>
                    <installDirectory>temp</installDirectory>
                </configuration>
                <executions>
                    <!-- It will install nodejs and npm -->
                    <execution>
                        <id>install node and npm</id>
                        <goals>
                            <goal>install-node-and-npm</goal>
                        </goals>
                        <configuration>
                            <nodeVersion>v10.6.0</nodeVersion>
                            <npmVersion>6.1.0</npmVersion>
                        </configuration>
                    </execution>

                    <!-- It will execute command "npm install" in the above configured directory -->
                    <execution>
                        <id>npm install</id>
                        <goals>
                            <goal>npm</goal>
                        </goals>
                        <configuration>
                            <arguments>install</arguments>
                        </configuration>
                    </execution>

                    <!-- It will execute command "npm build" inside "/angular" directory to clean and create "/dist" directory-->
                    <execution>
                        <id>npm build</id>
                        <goals>
                            <goal>npm</goal>
                        </goals>
                        <configuration>
                            <arguments>run build</arguments>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            [...]
        </plugins>
        [...]
    </build>
    [...]
</project>
```
