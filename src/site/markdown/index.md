About
====================
The Nuclos Source Maven plugin allows you to extract objects from the Nuclet data structure during a Maven build in 
order to do anything you would like to to with it. Since the Nuclos mechanisms allow you to create custom extensions and
build your own business logic in so called rules and web addons, you maybe want to handle these pieces of code with 
extra care like performing static source code analysis.

This plugin let's you call certain goals to write out different kind of objects to a target directory. Currently you may
especially apply it to write out web addon files in order to build the web client.

A Mojo for writing out server rules exists, but is not yet ready for usage since all of the depending objects are not
available (state machines, business objects, etc.)
